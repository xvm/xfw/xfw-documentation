Pull Request
============

Создание форка
--------------

1. Регистрируемся на bitbucket.org https://bitbucket.org/account/signup/
2. Переходим на страницу репозитория XFW https://bitbucket.org/XVM/xfw/
3. Создаем собственную копию репозитория посредством кнопки Fork

Создание локальной копии репозитория
------------------------------------

1. Устанавливаем TortoiseHg http://tortoisehg.bitbucket.org/
2. В TortoiseHg, в `файл-настройки-фиксация`, заполняем имя по шаблону `Name <email>`
3. Выполняем `файл-клонировать хранилище`. Указываем `https://bitbucket.org/xvm/xfw` как источник и любую папку на вашем жестком диск как назначение.

Заливка изменений в форк
------------------------

1. Вносим изменения в файлы XFW
2. Вводим сообщение для коммита и выбираем файлы, которые необходимо закоммитить. Жмем кнопку "Фиксировать"
3. Идем на вкладку синхронизация, вбиваем имя страницы вашего форка в поле, жмем кнопку сохранить (стрелка с диском), подтверждаем замену.
4. Жмем кнопку "Протолкнуть" (четвёртая на панели), соглашаемся. Вводим логин и пароль.

Создание Pull Request
---------------------

1. Переходим, на страницу вашего форка и жмем кнопку `pull request`
2. Заполняем форму, жмем кнопку `create pull request`
3. Всё! Дожидаемся принятия изменений в основную кодовую базу.
