
**********
Cборка XFW
**********

XVM Framework имеет несколько возможностей для сборки

* Сборка всего проекта с помощью скрипта ``/build.sh``;
* Сборка отдельных частей проекта с помощью скриптов:
   * ``/src/actionscript/build.sh``
   * ``/src/python/build.sh``
   * ``/src/swf/build.sh``


Конфигурация
============

Для настройки поведение сборочных скриптов можно использовать переменные среды:


Actionscript
------------
* ``$xfw_OS`` : Текущая ОС. Доступные значения: ``Linux``, ``Windows``
* ``$xfw_arch`` : Архитектура. Доступные значения: ``i386``, ``amd64``
* ``$xfw_mono`` : Интепретатор .NET. По умолчанию, ``пусто`` для Windows и ``mono`` для Linux
* ``$FLEX_HOME`` : Путь к Apache Flex SDK. По умолчанию, ``/opt/apache-flex`` для Linux и ``$LOCALAPPDATA/FlashDevelop/Apps/flexsdk/4.6.0`` для Windows
* ``$PLAYERGLOBAL_HOME`` : Путь к ``playerglobal.swc``. По умолчанию, ``$FLEX_HOME/frameworks/libs/player``


Python
------
* ``$XVMBUILD_PYTHON_FILEPATH``: Путь к исполняемому файлу интерпретатора Python версии 2.7. По умолчанию, ``/c/Python27/python.exe`` для Windows, если его нет, то ``python2.7`` из ``$PATH``
* ``$XFW_DEVELOPMENT`` : если установлено в 1, то библиотеки не будут собираться. Смотрите также ``$BUILD_LIBS``
* ``$BUILD_LIBS`` : если установлено в 1, то библиотеки будут собираться, даже если установлено ``$XFW_DEVELOPMENT``
* ``$CLEAR`` : очистить выходную директорию перед сборкой

