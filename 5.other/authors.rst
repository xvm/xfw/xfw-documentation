Авторы
======

Над проектами XVM и XVM Framework работали:

Команда XVM
-----------

* sirmax (Maxim Schedriviy)
* ibat (Alexander Bulychev)
* q4x2 (Sergey Kuznetsov)
* mr.a (Alexei Nikitin)
* mr13 (Oleg Savchenko)
* assassik (Maca Pavel)
* helkar (Yaroslav Brustinov)
* seriych (Sergey Churin)
* yukikaze (Alexander Putin)
* mixaill (Mikhail Paulyshka)
* XlebniDizele4ku (Ilya Litvinov)
* arzakon.nn

Волонтёры
---------

* Edgar Fournival
* night_dragon_on (Roman Astafev)
* Phantasm (Shamil Kerimov)
* Kotyarko_O (Dmitriy Balaganskiy)
* STL1te (Kirill Malyshev)
* sech_92 (Alexander Selezniov)
* vlad_cs_sr (Vladislav Degtyaryov)
* Polar Fox (Anton Khokhlov)
* Duv21 (Ilya Voronin)
* P0LIR0ID (Andrey Andruschyshyn)
* Alexander Podzorov
* demon2597
* johnp
* emhmk2
* lweibj
* Ktulho
* cany
* b1Ack WOT
* unstutby
* abagrov
* Quaksen
* plxplx
* lemaxho
* HorridoJoho
* chas125

